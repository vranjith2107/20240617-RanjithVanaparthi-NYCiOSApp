//
//  NYCSchoolDetailViewModelTest.swift
//  20240617-RanjithVanaparthi-NYCSchoolsTests
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation
import XCTest
import Combine
@testable import _0240617_RanjithVanaparthi_NYCSchools

class NYCSchoolDetailViewModelTests: XCTestCase {

    var viewModel: NYCSchoolDetailViewModel!
    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        cancellables = []
    }

    override func tearDown() {
        viewModel = nil
        cancellables = nil
        super.tearDown()
    }
    
    func testGetSchoolTestInfo_Success() {
        // JSON string representing the test data
        let jsonString = """
        [
            {
                "dbn": "12345",
                "school_name": "Test School 1",
                "num_of_sat_test_takers": "100",
                "sat_critical_reading_avg_score": "400",
                "sat_math_avg_score": "450",
                "sat_writing_avg_score": "410"
            },
            {
                "dbn": "67890",
                "school_name": "Test School 2",
                "num_of_sat_test_takers": "150",
                "sat_critical_reading_avg_score": "410",
                "sat_math_avg_score": "460",
                "sat_writing_avg_score": "420"
            }
        ]
        """
        
        let mockTestResults = decodeTestResults(from: jsonString)
        let service = MockSchoolDetailServiceRequest(testResults: mockTestResults, failureError: nil)
        viewModel = NYCSchoolDetailViewModel(service: service)

        let expectation = self.expectation(description: "Fetching school test results succeeds and sets UI state to loaded")
        
        viewModel.$uistate
            .dropFirst() // Skip initial loading state
            .sink { state in
                XCTAssertEqual(state, .loading)
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        viewModel.getSchoolTestInfo(dbnId: "12345")
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // Helper function to decode JSON string into an array of SchoolTestResult objects
    private func decodeTestResults(from jsonString: String) -> [SchoolTestResult] {
        let data = jsonString.data(using: .utf8)!
        let decoder = JSONDecoder()
        do {
            return try decoder.decode([SchoolTestResult].self, from: data)
        } catch {
            XCTFail("Failed to decode JSON data: \(error)")
            return []
        }
    }
}

// Mock service class to simulate network responses
class MockSchoolDetailServiceRequest: NYCSchoolServiceRequest {
    
    private let testResults: [SchoolTestResult]?
    private let failureError: NetworkError?
    
    init(testResults: [SchoolTestResult]?, failureError: NetworkError?) {
        self.testResults = testResults
        self.failureError = failureError
        super.init(networkRequest: RequestableManager(), environment: .development)
    }
    
    override func getSchoolTestResult(dbnId: String) -> AnyPublisher<[SchoolTestResult], NetworkError> {
        if let testResults = testResults {
            return Just(testResults)
                .setFailureType(to: NetworkError.self)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: failureError!)
                .eraseToAnyPublisher()
        }
    }
}
