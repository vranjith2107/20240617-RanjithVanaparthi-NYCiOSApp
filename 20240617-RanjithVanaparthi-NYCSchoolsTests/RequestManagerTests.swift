//
//  RequestManagerTests.swift
//  20240617-RanjithVanaparthi-NYCSchoolsTests
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation
import XCTest
import Combine
@testable import _0240617_RanjithVanaparthi_NYCSchools

class RequestableManagerTests: XCTestCase {

    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        cancellables = []
    }

    override func tearDown() {
        cancellables = nil
        super.tearDown()
    }
    
    func testRequest_SuccessfulResponse() {
        // Mock URLSession
        let url = URL(string: "https://example.com")!
        let responseData = "{\"message\":\"Success\"}".data(using: .utf8)!
        let urlResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
        
        URLProtocolMock.requestHandler = { request in
            return (urlResponse, responseData)
        }
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        
        let requestableManager = RequestableManager(session: session)
        let request = NetworkRequest(url: "https://example.com", httpMethod: .GET)
        
        let expectation = self.expectation(description: "Successful response")
        
        requestableManager.request(request)
            .sink(receiveCompletion: { completion in
                if case .failure(let error) = completion {
                    XCTFail("Request failed with error: \(error)")
                }
            }, receiveValue: { (response: DummyResponse) in
                XCTAssertEqual(response.message, "Success")
                expectation.fulfill()
            })
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func testRequest_BadURL() {
        let requestableManager = RequestableManager()
        let request = NetworkRequest(url: "Invalid URL", httpMethod: .GET)
        
        let expectation = self.expectation(description: "Bad URL error")
        
        requestableManager.request(request)
            .sink(receiveCompletion: { completion in
                if case .failure(let error) = completion {
                    XCTAssertEqual(error, NetworkError.badURL("Invalid Url"))
                    expectation.fulfill()
                }
            }, receiveValue: { (response: DummyResponse) in
                XCTFail("Request should not succeed")
            })
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}

// Dummy response structure for testing
struct DummyResponse: Codable {
    let message: String
}

// URLProtocolMock to mock URLSession
class URLProtocolMock: URLProtocol {
    
    static var requestHandler: ((URLRequest) -> (HTTPURLResponse, Data))?
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        guard let handler = URLProtocolMock.requestHandler else {
            fatalError("Handler is unavailable.")
        }
        
        let (response, data) = handler(request)
        
        client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        client?.urlProtocol(self, didLoad: data)
        client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {}
}
