//
//  NetworkingManagerTests.swift
//  20240617-RanjithVanaparthi-NYCSchoolsTests
//
//  Created by Ranjith vanaparthi on 6/17/24.
//


import XCTest
import Combine
import Foundation
@testable import _0240617_RanjithVanaparthi_NYCSchools

class NetworkTests: XCTestCase {
    
    func testNetworkRequestInitWithEncodable() {
        struct TestBody: Encodable {
            let value: String
        }
        
        let url = "https://example.com"
        let headers = ["Content-Type": "application/json"]
        let body = TestBody(value: "test")
        let httpMethod: HTTPMethod = .POST
        let timeout: Float = 30.0
        
        let networkRequest = NetworkRequest(url: url, headers: headers, reqBody: body, reqTimeout: timeout, httpMethod: httpMethod)
        
        XCTAssertEqual(networkRequest.url, url)
        XCTAssertEqual(networkRequest.headers, headers)
        XCTAssertNotNil(networkRequest.body)
        XCTAssertEqual(networkRequest.requestTimeOut, timeout)
        XCTAssertEqual(networkRequest.httpMethod, httpMethod)
    }
    
    func testNetworkRequestInitWithData() {
        let url = "https://example.com"
        let headers = ["Content-Type": "application/json"]
        let body = "test".data(using: .utf8)
        let httpMethod: HTTPMethod = .POST
        let timeout: Float = 30.0
        
        let networkRequest = NetworkRequest(url: url, headers: headers, reqBody: body, reqTimeout: timeout, httpMethod: httpMethod)
        
        XCTAssertEqual(networkRequest.url, url)
        XCTAssertEqual(networkRequest.headers, headers)
        XCTAssertEqual(networkRequest.body, body)
        XCTAssertEqual(networkRequest.requestTimeOut, timeout)
        XCTAssertEqual(networkRequest.httpMethod, httpMethod)
    }
    
    func testBuildURLRequest() {
        let url = "https://example.com"
        let headers = ["Content-Type": "application/json"]
        let body = "test".data(using: .utf8)
        let httpMethod: HTTPMethod = .POST
        
        let networkRequest = NetworkRequest(url: url, headers: headers, reqBody: body, httpMethod: httpMethod)
        let urlRequest = networkRequest.buildURLRequest(with: URL(string: url)!)
        
        XCTAssertEqual(urlRequest.url?.absoluteString, url)
        XCTAssertEqual(urlRequest.allHTTPHeaderFields, headers)
        XCTAssertEqual(urlRequest.httpBody, body)
        XCTAssertEqual(urlRequest.httpMethod, httpMethod.rawValue)
    }
    
    func testNetworkErrorEquality() {
        let error1 = NetworkError.badURL("Bad URL")
        let error2 = NetworkError.badURL("Bad URL")
        
        XCTAssertEqual(error1, error2)
        
        let error3 = NetworkError.apiError(code: 404, error: "Not Found")
        let error4 = NetworkError.apiError(code: 404, error: "Not Found")
        
        XCTAssertEqual(error3, error4)
        
        let error5 = NetworkError.badRequest(code: 400, error: "Bad Request")
        let error6 = NetworkError.badRequest(code: 400, error: "Bad Request")
        
        XCTAssertEqual(error5, error6)
    }
    
    func testRequestableMock() {
        class MockRequestable: Requestable {
            var requestTimeOut: Float = 30.0
            
            func request<T>(_ req: NetworkRequest) -> AnyPublisher<T, NetworkError> where T : Decodable, T : Encodable {
                let dummyData = "{}".data(using: .utf8)!
                return Just(dummyData)
                    .tryMap { data in
                        return try JSONDecoder().decode(T.self, from: data)
                    }
                    .mapError { _ in NetworkError.invalidJSON("Invalid JSON") }
                    .eraseToAnyPublisher()
            }
        }
        
        struct DummyResponse: Codable {
            let message: String
        }
        
        let mockRequestable = MockRequestable()
        let url = "https://example.com"
        let networkRequest = NetworkRequest(url: url, httpMethod: .GET)
        
        let expectation = self.expectation(description: "Request should complete")
        
        let cancellable = mockRequestable.request(networkRequest)
            .sink(receiveCompletion: { completion in
                if case .failure(let error) = completion {
                    XCTFail("Request failed with error: \(error)")
                }
                expectation.fulfill()
            }, receiveValue: { (response: DummyResponse) in
                XCTAssertEqual(response.message, "Success")
            })
        
        waitForExpectations(timeout: 5.0, handler: nil)
        cancellable.cancel()
    }
}
