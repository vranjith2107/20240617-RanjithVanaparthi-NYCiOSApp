//
//  NYCEndPointsLinksTests.swift
//  20240617-RanjithVanaparthi-NYCSchoolsTests
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation
import XCTest
@testable import _0240617_RanjithVanaparthi_NYCSchools

class NYCSchoolEndpointsTests: XCTestCase {

    func testListOfSchoolsInfoURL() {
        let endpoint = NYCSchoolEndpoints.listOfSchoolsInfo
        let environment = Environment.development
        let expectedURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        
        XCTAssertEqual(endpoint.getURL(from: environment), expectedURL)
    }
    
    func testGetSchoolInfoURL() {
        let dbnId = "12345"
        let endpoint = NYCSchoolEndpoints.getSchoolInfo(dbnId: dbnId)
        let environment = Environment.development
        let expectedURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=12345"
        
        XCTAssertEqual(endpoint.getURL(from: environment), expectedURL)
    }
    
    func testHTTPMethod() {
        XCTAssertEqual(NYCSchoolEndpoints.listOfSchoolsInfo.httpMethod, .GET)
        XCTAssertEqual(NYCSchoolEndpoints.getSchoolInfo(dbnId: "12345").httpMethod, .GET)
    }
    
    func testRequestBody() {
        XCTAssertNil(NYCSchoolEndpoints.listOfSchoolsInfo.requestBody)
        XCTAssertNil(NYCSchoolEndpoints.getSchoolInfo(dbnId: "12345").requestBody)
    }
    
    func testRequestTimeout() {
        XCTAssertEqual(NYCSchoolEndpoints.listOfSchoolsInfo.requestTimeOut, 20)
        XCTAssertEqual(NYCSchoolEndpoints.getSchoolInfo(dbnId: "12345").requestTimeOut, 20)
    }
    
    func testCreateRequest() {
        let endpoint = NYCSchoolEndpoints.listOfSchoolsInfo
        let environment = Environment.development
        let request = endpoint.createRequest(environment: environment)
        
        XCTAssertEqual(request.url, "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertNil(request.body)
        XCTAssertEqual(request.httpMethod, .GET)
    }
}
