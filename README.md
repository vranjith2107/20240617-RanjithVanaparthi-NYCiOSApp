
# NYC School App

The NYC School App provides information about New York City schools, including general school details and specific test results. It is designed to help users easily access and search for school information.

## Features

- **List of Schools**: Browse a list of NYC schools with basic details.
- **School Details**: View detailed information about individual schools.
- **Test Results**: Access SAT test results for specific schools.
- **Search**: Search for schools by name.


## Requirements

- iOS 16.0+
- Xcode 15.0+
- Swift 5.0+
