//
//  NYCSchoolServiceRequest.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation
import Combine

protocol NYCSchoolServiceable {
    func listOfSchoolInfo() -> AnyPublisher<[School], NetworkError>
    func getSchoolTestResult(dbnId: String) -> AnyPublisher<[SchoolTestResult], NetworkError>
}

class NYCSchoolServiceRequest: NYCSchoolServiceable {
    
    private var networkRequest: Requestable
    private var environment: Environment = .development
    
  // inject this for testability
    init(networkRequest: Requestable, environment: Environment) {
        self.networkRequest = networkRequest
        self.environment = environment
    }
    
    // List of schools info service request
    func listOfSchoolInfo() -> AnyPublisher<[School], NetworkError> {
        let endpoint = NYCSchoolEndpoints.listOfSchoolsInfo
        let request = endpoint.createRequest(environment: self.environment)
        
        return self.networkRequest.request(request)
    }
    
    //Get school test score by ID
    func getSchoolTestResult(dbnId: String) -> AnyPublisher<[SchoolTestResult], NetworkError> {
        let endpoint = NYCSchoolEndpoints.getSchoolInfo(dbnId: dbnId)
        let request = endpoint.createRequest(environment: environment)
        return self.networkRequest.request(request)
    }
  
}


