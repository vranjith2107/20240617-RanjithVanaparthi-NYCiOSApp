//
//  SchoolTestResult.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation


struct SchoolTestResult: Codable {
    let dbn: String
    let num_of_sat_test_takers: String?
    
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}
