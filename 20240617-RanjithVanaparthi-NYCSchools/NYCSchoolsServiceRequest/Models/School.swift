//
//  School.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation

struct School: Codable {
    let dbn: String
    let school_name: String
    let overview_paragraph: String
    let total_students: String?
    let graduation_rate: String?
    let attendance_rate: String?
    let website: String?
    let school_email: String?
    let phone_number: String?
    let latitude: String?
    let longitude: String?
    // Add other fields as needed
}

extension School: Equatable {}
