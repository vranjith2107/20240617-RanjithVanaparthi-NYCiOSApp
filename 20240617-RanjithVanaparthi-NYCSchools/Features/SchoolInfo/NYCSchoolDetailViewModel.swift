//
//  NYCSchoolDetailViewModel.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation
import Combine

// NYCSchoolDetailViewModel is responsible for fetching and managing the state of school test results.
class NYCSchoolDetailViewModel: ObservableObject {
    // Published properties to notify the view of changes, allowing the UI to react and update accordingly.
    @Published var schoolTestResults = [SchoolTestResult]() // Stores the list of school test results.
    @Published var uistate: UIState = .loading // Tracks the current UI state to control view rendering.

    // Enum to define possible UI states, making state management clear and explicit.
    enum UIState {
        case loading // Indicates data is currently being fetched.
        case error // Indicates an error occurred during data fetching.
        case loaded // Indicates data has been successfully loaded and is ready to display.
    }
    
    private let service: NYCSchoolServiceRequest
    init(service: NYCSchoolServiceRequest = NYCSchoolServiceRequest(networkRequest: RequestableManager(), environment: .development)) {
           self.service = service
    }
    
    // Set to hold any cancellables, used for managing memory and lifecycle of network requests.
    var subscriptions = Set<AnyCancellable>()
    
    // Function to initiate fetching of school test results for a given DBN (Database Number).
    func getSchoolTestInfo(dbnId: String) {
        uistate = .loading // Set the state to loading before starting the fetch.
        service.getSchoolTestResult(dbnId: dbnId)
            .receive(on: RunLoop.main) // Ensure the UI updates are performed on the main thread.
            .sink { (completion) in // Handles the response of the network request.
                switch completion {
                case .failure(let error): // In case of failure, print error and set state to error.
                    self.uistate = .error
                case .finished: // On successful completion, update the UI state to loaded.
                    self.uistate = .loaded
                }
            } receiveValue: { (response) in // On receiving data, store it in schoolTestResults.
                self.schoolTestResults = response
            }
            .store(in: &subscriptions) // Store the subscription to manage its lifecycle.
    }
    
}
