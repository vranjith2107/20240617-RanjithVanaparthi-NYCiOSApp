//
//  NYCSchoolListViewModel.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation
import Combine

// SchoolListViewModel is responsible for managing the list of schools, handling the search functionality, and managing the UI state.
class SchoolListViewModel: ObservableObject {
    // Published properties to notify the view of changes, allowing the UI to update reactively.
    @Published var schools: [School] = [] // Stores the list of schools.
    @Published var searchString = "" // Stores the current search string input by the user.
    @Published var uistate: UIState = .loading // Tracks the current UI state to control view rendering.
    
    // Enum to define possible UI states, making state management clear and explicit.
    enum UIState {
        case loading // Indicates data is currently being fetched.
        case error // Indicates an error occurred during data fetching.
        case loaded // Indicates data has been successfully loaded and is ready to display.
    }
    
    // Constructor initializes the ViewModel by loading schools.
    private let service: NYCSchoolServiceRequest

    init(service: NYCSchoolServiceRequest = NYCSchoolServiceRequest(networkRequest: RequestableManager(), environment: .development)) {
           self.service = service
           loadSchools()
    }

    // Set to hold any cancellables, used for managing memory and lifecycle of network requests.
    var subscriptions = Set<AnyCancellable>()

    // Function to initiate fetching of schools from the network.
    func loadSchools() {
        uistate = .loading // Set the state to loading before starting the fetch.
        service.listOfSchoolInfo()
            .receive(on: RunLoop.main) // Ensure the UI updates are performed on the main thread.
            .sink { (completion) in // Handles the response of the network request.
                switch completion {
                case .failure(_): // In case of failure, print error and set state to error.
                    self.uistate = .error
                case .finished: // On successful completion, update the UI state to loaded.
                    self.uistate = .loaded
                }
            } receiveValue: { (response) in
                // On receiving data, store it in schools.
                self.schools = response
            }
            .store(in: &subscriptions) // Store the subscription to manage its lifecycle.
    }
    
    // Function to filter schools based on the current search string.
    func search() -> [School] {
        if !searchString.isEmpty {
            // Filters schools to include only those that match the search string.
            return schools.filter { $0.school_name.lowercased().contains(searchString.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)) }
        } else {
            // If search string is empty, return the full list of schools.
            return schools
        }
    }
}
