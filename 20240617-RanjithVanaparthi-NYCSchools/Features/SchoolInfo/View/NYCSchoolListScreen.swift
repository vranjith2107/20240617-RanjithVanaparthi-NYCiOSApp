//
//  NYCSchoolListScreen.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import SwiftUI

// NYCSchoolListScreen is a SwiftUI View that presents a list of high schools using navigation and search functionality.
struct NYCSchoolListScreen: View {
    // ViewModel managing the state and data for the list of schools.
    @StateObject private var viewModel = SchoolListViewModel()

    // The body of the view where the UI elements are laid out.
    var body: some View {
        NavigationStack {
            Group {
                // Switch based on the state of the ViewModel to determine what to display.
                switch viewModel.uistate {
                case .loading:
                    // Display a progress view when the data is loading.
                    ProgressView("Loading...")
                        .progressViewStyle(CircularProgressViewStyle())
                case .error:
                    // Display an error view with a retry option if there is an error.
                    ErrorView {
                        viewModel.loadSchools()
                    }
                case .loaded:
                    // Display the list of schools if data is successfully loaded.
                    List {
                        ForEach(viewModel.search(), id: \.dbn) { school in
                            NavigationLink {
                                NYCSchoolDetailScreen(school: school)
                            } label: {
                                SchoolListItem(school: school)
                            }
                        }
                    }
                }
            }
            .listStyle(.plain) // Applies a plain list style.
            .navigationTitle("Schools List") // Sets the title for the navigation bar.
            .navigationBarTitleDisplayMode(.inline) // Determines the style of the navigation title.
            .searchable(text: $viewModel.searchString) // Adds a search bar bound to the ViewModel's searchString.
            .toolbar {
                navigationTitle // Defines the content of the toolbar.
            }
        }
    }
    
    // ToolbarContentBuilder to customize the navigation bar content.
    @ToolbarContentBuilder
    var navigationTitle: some ToolbarContent {
        ToolbarItem(placement: .principal) {
            Text("")
        }
        ToolbarItem(placement: .navigationBarLeading) {
            // Sets the title and style of the navigation bar leading item.
            Text("High schools")
                .font(.system(.largeTitle, design: .rounded, weight: .bold))
                .foregroundColor(.blue)
                .padding(.bottom, 6)
        }
    }
}

// Extensions or additional functionalities for NYCSchoolListScreen can be defined here.

// Represents an individual school item in the list.
struct SchoolListItem: View {
    let school: School
    
    var body: some View {
        // Display the school name with specific font and color settings.
        Text(school.school_name)
            .lineLimit(1) // Limits the display to a single line.
            .font(.system(.title3, design: .rounded))
            .foregroundColor(.blue)
    }
}

// View representing an error state with an option to retry fetching the data.
struct ErrorView: View {
    let onRetry: () -> Void // Closure to be called when retry is initiated.
    
    var body: some View {
        VStack {
            Text("Something went wrong")
                .foregroundColor(.red)
            Button("Retry") {
                onRetry()
            }
        }
    }
}
