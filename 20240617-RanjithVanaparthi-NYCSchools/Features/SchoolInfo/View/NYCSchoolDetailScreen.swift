//
//  NYCSchoolDetailScreen.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import SwiftUI
import Charts

struct NYCSchoolDetailScreen: View {
    // Property to store school information passed to this view.
    let school: School
    
    // ViewModel instance to manage data and UI state.
    @StateObject private var viewModel = NYCSchoolDetailViewModel()
    
    // The body of the view where the UI elements are laid out.
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            VStack(spacing: 0) {
                Text(school.school_name)
                    .font(.system(.headline, design: .rounded))
                    .foregroundColor(.blue)
                    .lineLimit(2)
                    .multilineTextAlignment(.leading)
                    .minimumScaleFactor(0.5)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                    .padding(.vertical, 6)
                Divider()
            }
            .background(Material.thick)
            ScrollView {
                
                switch viewModel.uistate {
                case .loading:
                    ProgressView("Loading...")
                        .progressViewStyle(CircularProgressViewStyle())
                case .error:
                    ErrorView {
                        viewModel.getSchoolTestInfo(dbnId: school.dbn)
                    }
                case .loaded:
                    VStack(alignment: .leading, spacing: 16) {
                        basicInfo
                        satInfo
                        links
                        Spacer()
                    }
                    .padding()
                }
            }
        }
        .onAppear {
            // Trigger data fetching when the view appears.
            viewModel.getSchoolTestInfo(dbnId: school.dbn)
        }
    }
    
    // View for displaying links like the school's website, email, phone, and location on a map.
    var links: some View {
        return VStack(alignment: .leading, spacing: 16) {
            Divider()
            Text("School Links")
                .textCase(.uppercase)
                .font(.headline)
                .foregroundColor(.blue)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .bottom, spacing: 32) {
                   
                    if let email = school.school_email, let url = URL(string: "mailto:\(email)") {
                        Link(destination: url) {
                            VStack {
                                Image(systemName: "envelope.fill")
                                    .imageScale(.large)
                                    .fontWeight(.light)
                                    .foregroundColor(.orange)
                                    .frame(width: 40, height: 40)
                                    .background {
                                        RoundedRectangle(cornerRadius: 8, style: .continuous).stroke(.orange, lineWidth: 2)
                                    }
                                Text("Email")
                            }
                        }
                    }
                    
                    if let phone = school.phone_number, let url = URL(string: "tel:\(phone)") {
                        Link(destination: url) {
                            VStack {
                                Image(systemName: "phone.fill")
                                    .imageScale(.large)
                                    .fontWeight(.light)
                                    .foregroundStyle(.green)
                                    .frame(width: 40, height: 40)
                                    .background {
                                        RoundedRectangle(cornerRadius: 8, style: .continuous).stroke(.green, lineWidth: 2)
                                    }
                                Text("Phone")
                            }
                        }
                    }
                    
                    if let latitude = school.latitude, let longitude = school.longitude {
                        var urlComponents = URLComponents(string: "http://maps.apple.com/?ll=\(latitude),\(longitude)&z=18")
                        let _ = urlComponents?.queryItems?.append(URLQueryItem(name: "q", value: school.school_name))
                        if let url = urlComponents?.url {
                            
                            Link(destination: url) {
                                VStack {
                                    Image(systemName: "map.fill")
                                        .imageScale(.large)
                                        .fontWeight(.light)
                                        .foregroundStyle(Color.brown)
                                        .frame(width: 40, height: 40)
                                        .background {
                                            RoundedRectangle(cornerRadius: 8, style: .continuous).stroke(Color.brown, lineWidth: 2)
                                        }
                                    Text("Map")
                                }
                            }
                        }
                    }
                    
                    Spacer()
                }
                .padding(.top, 4)
            }
        }
    }
    
    // Helper views for displaying basic information and SAT scores.
    @ViewBuilder
    var basicInfo: some View {
        Divider()
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .lastTextBaseline, spacing: 24) {
                TableViewInfo("Students", statistic: Int(school.total_students ?? "0"), format: .number.notation(.compactName))
                TableViewInfo("Grad. rate", statistic: Double(school.graduation_rate ?? "0"), format: .percent.rounded(rule: .up, increment: 1))
                TableViewInfo("Attend. rate", statistic: Double(school.attendance_rate ?? "0"), format: .percent.rounded(rule: .up, increment: 1))
                Spacer()
            }
        }
    }
    
    @ViewBuilder
    var satInfo: some View {
        Divider()
        VStack(alignment: .leading, spacing: 16) {
            Text("SAT Scores")
                .textCase(.uppercase)
                .font(.headline)
                .foregroundColor(.blue)

            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .bottom, spacing: 32) {
                    TableViewInfo("Reading", statistic: Int(viewModel.schoolTestResults.first?.sat_critical_reading_avg_score ?? "0"), format: .number.notation(.compactName))
                    TableViewInfo("Math", statistic: Int(viewModel.schoolTestResults.first?.sat_math_avg_score ?? "0"), format: .number.notation(.compactName))
                    TableViewInfo("Writing", statistic: Int(viewModel.schoolTestResults.first?.sat_writing_avg_score ?? "0"), format: .number.notation(.compactName))
                    Spacer()
                }
            }
        }
    }
}


struct TableViewInfo<Label, Content>: View where Label : View, Content : View {
    let label: Label
    let content: Content
    
    init(@ViewBuilder content: () -> Content, @ViewBuilder label: () -> Label) {
        self.label = label()
        self.content = content()
    }
    
    var body: some View {
        Group {
            LabeledContent {
                content
            } label: {
                label
            }
        }
        .labeledContentStyle(.vertical)
    }
}

// Extensions and custom styles are defined to customize the appearance of labeled content.
extension TableViewInfo where Label == Text, Content == Text {
    init<S, F>(_ label: S, statistic: F.FormatInput?, format: F) where S : StringProtocol, F : FormatStyle, F.FormatInput : Equatable, F.FormatOutput == String {
        self.label = Text(label)
        
        if let statistic {
            self.content = Text(statistic, format: format)
        } else {
            self.content = Self.placeholder
        }
    }
    
    init<S>(_ label: S, statistic: S?) where S : StringProtocol {
        self.label = Text(label)
        if let statistic {
            self.content = Text(statistic)
        } else {
            self.content = Self.placeholder
        }
    }
    
    private static var placeholder: Text {
        Text("—")
    }
}

struct VerticalLabeledContentStyle: LabeledContentStyle {
    func makeBody(configuration: Configuration) -> some View {
        VStack(alignment: .leading, spacing: 4) {
            configuration.label
                .foregroundColor(.secondary)
                .lineLimit(2)
                .minimumScaleFactor(0.8)
            configuration.content
                .font(.system(.title, design: .rounded, weight: .semibold))
                .foregroundColor(.blue)
                .lineLimit(1)
                .minimumScaleFactor(0.5)
        }
    }
}

extension LabeledContentStyle where Self == VerticalLabeledContentStyle {
    static var vertical: Self { Self() }
}
