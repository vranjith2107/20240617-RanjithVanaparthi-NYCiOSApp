//
//  _0240617_RanjithVanaparthi_NYCSchoolsApp.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import SwiftUI

@main
struct _0240617_RanjithVanaparthi_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            NYCSchoolListScreen()
        }
    }
}
