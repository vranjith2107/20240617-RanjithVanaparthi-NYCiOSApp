//
//  Requestable.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Combine
import Foundation

// Defines a protocol for requestable objects, which should be capable of making network requests.
protocol Requestable {
    // Timeout for the request in seconds.
    var requestTimeOut: Float { get }
    
    // Generic method to make a network request returning a publisher for asynchronous handling.
    func request<T: Codable>(_ req: NetworkRequest) -> AnyPublisher<T, NetworkError>
}

// Struct to encapsulate the details of a network request.
struct NetworkRequest {
    // URL of the request.
    let url: String
    // Optional dictionary for HTTP headers.
    let headers: [String: String]?
    // Optional data for the HTTP body, typically used for POST/PUT requests.
    let body: Data?
    // Optional custom request timeout.
    let requestTimeOut: Float?
    // HTTP method type.
    let httpMethod: HTTPMethod
    
    // Initializer for creating a request from an encodable object as the body.
    init(url: String,
         headers: [String: String]? = nil,
         reqBody: Encodable? = nil,
         reqTimeout: Float? = nil,
         httpMethod: HTTPMethod) {
        self.url = url
        self.headers = headers
        self.body = reqBody?.encode()
        self.requestTimeOut = reqTimeout
        self.httpMethod = httpMethod
    }
    
    // Initializer for creating a request with raw data as the body.
    init(url: String,
         headers: [String: String]? = nil,
         reqBody: Data? = nil,
         reqTimeout: Float? = nil,
         httpMethod: HTTPMethod) {
        self.url = url
        self.headers = headers
        self.body = reqBody
        self.requestTimeOut = reqTimeout
        self.httpMethod = httpMethod
    }
    
    // Helper method to build a URLRequest from the current NetworkRequest instance.
    func buildURLRequest(with url: URL) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpBody = body
        return urlRequest
    }
}

// Enumeration to define HTTP methods.
enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
}

// Enumeration to define various network errors.
enum NetworkError: Error, Equatable {
    case badURL(_ error: String)
    case apiError(code: Int, error: String)
    case invalidJSON(_ error: String)
    case unauthorized(code: Int, error: String)
    case badRequest(code: Int, error: String)
    case serverError(code: Int, error: String)
    case noResponse(_ error: String)
    case unableToParseData(_ error: String)
    case unknown(code: Int, error: String)
}

// Extension to provide a method to encode any Encodable object into Data, using JSON encoding.
extension Encodable {
    func encode() -> Data? {
        do {
            return try JSONEncoder().encode(self)
        } catch {
            return nil // Return nil if encoding fails.
        }
    }
}
