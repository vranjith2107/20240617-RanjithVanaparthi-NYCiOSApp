//
//  RequestableManager.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//


import Combine
import Foundation

// Manager class that conforms to the Requestable protocol and handles network requests.
class RequestableManager: Requestable {
    
    // Default timeout for the network requests in seconds.
    var requestTimeOut: Float = 30
    
    // URLSession instance used for making network requests.
    private let session: URLSession
    
    // Initializer that allows injection of a custom URLSession, defaulting to the shared session.
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    // Function to perform a network request, returning a publisher for asynchronous handling.
    func request<T>(_ req: NetworkRequest) -> AnyPublisher<T, NetworkError>
    where T: Decodable, T: Encodable {
        
        // Configure the session with a timeout interval.
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = TimeInterval(req.requestTimeOut ?? requestTimeOut)
        
        // Ensure the URL string is valid.
        guard let url = URL(string: req.url) else {
            // Return a fail publisher if the URL is invalid.
            return AnyPublisher(
                Fail<T, NetworkError>(error: NetworkError.badURL("Invalid Url"))
            )
        }
        
        // Use the dataTaskPublisher from URLSession to create a publisher for the network request.
        return session
            .dataTaskPublisher(for: req.buildURLRequest(with: url))
            .tryMap { output in
                // Check if the response is an HTTPURLResponse, else throw an error.
                guard output.response is HTTPURLResponse else {
                    throw NetworkError.serverError(code: 0, error: "Server error")
                }
                return output.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                // Map decoding errors to NetworkError.invalidJSON.
                NetworkError.invalidJSON(String(describing: error))
            }
            .eraseToAnyPublisher()
    }
}
