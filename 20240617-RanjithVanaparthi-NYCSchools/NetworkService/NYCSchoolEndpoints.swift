//
//  NYCSchoolEndpoints.swift
//  20240617-RanjithVanaparthi-NYCSchools
//
//  Created by Ranjith vanaparthi on 6/17/24.
//

import Foundation

public typealias Headers = [String: String]

// if you wish you can have multiple services like this in a project
enum NYCSchoolEndpoints {
    
  // organise all the end points here for clarity
    case listOfSchoolsInfo
    case getSchoolInfo(dbnId: String)
    
    
  // gave a default timeout but can be different for each.
    var requestTimeOut: Int {
        return 20
    }
    
  //specify the type of HTTP request
    var httpMethod: HTTPMethod {
        switch self {
        case .listOfSchoolsInfo,
             .getSchoolInfo(_):
            return .GET
        }
    }
    
  // compose the NetworkRequest
    func createRequest(environment: Environment) -> NetworkRequest {
        var headers: Headers = [:]
        headers["Content-Type"] = "application/json"
        return NetworkRequest(url: getURL(from: environment), headers: headers, reqBody: requestBody, httpMethod: httpMethod)
    }
    
  // encodable request body for POST
    var requestBody: Encodable? {
        switch self {
        default:
            return nil
        }
    }
    
  // compose urls for each request
    func getURL(from environment: Environment) -> String {
        let baseUrl = environment.purchaseServiceBaseUrl
        switch self {
        case .listOfSchoolsInfo:
            return "\(baseUrl)/resource/s3k6-pzi2.json"
        case .getSchoolInfo(let dbnId):
            return "\(baseUrl)/resource/f9bf-2cp4.json?dbn=\(dbnId)"
        }
    }
}


public enum Environment: String, CaseIterable {
    case development
    case staging
    case production
}

extension Environment {
    var purchaseServiceBaseUrl: String {
        switch self {
        case .development:
            return "https://data.cityofnewyork.us"
        case .staging:
            return "https://data.cityofnewyork.us"
        case .production:
            return "https://data.cityofnewyork.us"
        }
    }
}
